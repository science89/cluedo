package univorleans.fr.projetcluedo.dao;

import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import univorleans.fr.projetcluedo.modele.Joueur;
import univorleans.fr.projetcluedo.modele.Partie;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "cluedo", path = "partie")

public interface PartieRepository extends MongoRepository<Partie, Integer> {

    @Override
    List<Partie> findAll();

    @Override
    Optional<Partie> findById(Integer id);

//    @Query("{etat: })")
//    List<Partie> findPartieByEtat(String etat);
}
