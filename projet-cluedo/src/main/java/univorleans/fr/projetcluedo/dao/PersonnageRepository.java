package univorleans.fr.projetcluedo.dao;

import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import univorleans.fr.projetcluedo.modele.Personnage;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "personnage", path = "personnage")
public interface PersonnageRepository extends MongoRepository<Personnage, Integer> {

    @Override
    List<Personnage> findAll();

    @Override
    Optional<Personnage> findById(Integer integer);

    @Override
    <S extends Personnage> boolean exists(Example<S> example);

    @Override
    <S extends Personnage> Optional<S> findOne(Example<S> example);
}
