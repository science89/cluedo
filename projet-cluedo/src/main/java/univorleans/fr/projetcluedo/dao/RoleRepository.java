package univorleans.fr.projetcluedo.dao;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import univorleans.fr.projetcluedo.modele.Role;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "role", path = "role")

public interface RoleRepository extends MongoRepository<Role, String> {

    Role findByRole(@Param("role") String role);

    List<Role> findAll();
}