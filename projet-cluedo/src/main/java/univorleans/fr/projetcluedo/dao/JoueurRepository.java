package univorleans.fr.projetcluedo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import univorleans.fr.projetcluedo.modele.Joueur;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "joueur", path = "joueur")

public interface JoueurRepository extends MongoRepository<Joueur, Integer> {

    Joueur findByPseudo(@Param("pseudo") String name);

    List<Joueur>findAll();

}
