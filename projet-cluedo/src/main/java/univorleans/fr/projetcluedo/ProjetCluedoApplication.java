package univorleans.fr.projetcluedo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import univorleans.fr.projetcluedo.dao.JoueurRepository;
import univorleans.fr.projetcluedo.dao.PartieRepository;
import univorleans.fr.projetcluedo.dao.RoleRepository;
import univorleans.fr.projetcluedo.modele.Joueur;
import univorleans.fr.projetcluedo.modele.Partie;
import univorleans.fr.projetcluedo.modele.Personnage;
import univorleans.fr.projetcluedo.modele.Role;
import univorleans.fr.projetcluedo.service.GestionCluedoImpl;
import univorleans.fr.projetcluedo.service.GestionCluedoInterface;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
@EnableWebSecurity
public class ProjetCluedoApplication  implements CommandLineRunner {

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    GestionCluedoImpl service;

    public static void main(String[] args) {
        SpringApplication.run(ProjetCluedoApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {


        /** I-Creation des roles **/

            roleRepository.save(new Role("1","USER"));
            roleRepository.save(new Role("2","ADMIN"));

        /** II-creation de 4 joueurs dont un admin **/

            /* ADMIN */
            Joueur admin = new Joueur("admin@gmail.com","admin", "admin");
            service.inscription(admin);

            /* JULIE */
            Joueur julie = new Joueur("julie@gmail.com","julie", "test");
            service.inscription(julie);

            /* PIERRE */
            Joueur pierre = new Joueur("pierre@gmail.com","pierre", "test");
            service.inscription(pierre);

            /* AXEL */
            Joueur axel = new Joueur("axel@gmail.com","axel", "test");

            service.inscription(axel);
            

        /** I-Lancement scenario admin **/

            /** 1->se logger en tant qu'admin **/
            service.connexion(admin);

            /** 2->Creation des personnages **/

            service.creerPersonnage(Personnage.NomPersonnage.MLLEROSE.toString());
            service.creerPersonnage(Personnage.NomPersonnage.COLONELMOUTARDE.toString());
            service.creerPersonnage(Personnage.NomPersonnage.DOCORCHIDEE.toString());
            service.creerPersonnage(Personnage.NomPersonnage.MMEPERVENCHE.toString());
            service.creerPersonnage(Personnage.NomPersonnage.MROLIVE.toString());
            service.creerPersonnage(Personnage.NomPersonnage.PROFVIOLET.toString());

            service.deconnexion(admin);

        /**II- Lancement scenario jeu **/

            /** 1-> connecter un joueur */

    		    service.connexion(julie);

            /** 2-> creer une partie avec le joueur connecte comme maitre **/

    		    Partie p1 = service.creerPartie();
                System.out.println("p = " + p1);

            /** 3-> inviter 2 joueurs (max 5) existant   a une partie **/

                 service.ajoutJoueur(1, pierre.getPseudo());
                 service.ajoutJoueur(1, axel.getPseudo());
                 System.out.println("partie -> " + service.getPartieById(1));

                 service.deconnexion(julie);


            /** 4- les invites rejoignent la partie **/

                service.connexion(axel);
                service.rejoindrePartie(1,axel.getPseudo());
                service.deconnexion(axel);

                service.connexion(pierre);
                service.rejoindrePartie(1,pierre.getPseudo());

                System.out.println("partie = " + service.getJoueursPartie(1));

        /** SAUVEGARDE D'UNE PARTIE **/

            // tous les joueurs doivent donner leur accord au prealable
        
            service.setAccordJoueur(1,pierre.getPseudo(),true);
            service.deconnexion(pierre);

            service.connexion(axel);
            service.setAccordJoueur(1,axel.getPseudo(),true);


            System.out.println("partie -> " + service.getPartieById(1));
            service.deconnexion(axel);


        /** 5-> se connecter en tant que maitre de la partie et suspendre la partie
         *  (possible qu'une fois que tous les joueurs ont donnes leur accord) **/
            service.connexion(julie);
    		service.sauvegarderPartie(1);

		/** 6- reprendre la partie **/

            service.reprendrePartie(1);
            System.out.println("partie -> " + service.getPartieById(1));
            service.deconnexion(julie);

    }
}
