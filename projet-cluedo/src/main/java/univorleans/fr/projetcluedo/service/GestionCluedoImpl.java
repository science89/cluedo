package univorleans.fr.projetcluedo.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import univorleans.fr.projetcluedo.dao.JoueurRepository;
import univorleans.fr.projetcluedo.dao.PersonnageRepository;
import univorleans.fr.projetcluedo.dao.RoleRepository;
import univorleans.fr.projetcluedo.dao.PartieRepository;
import univorleans.fr.projetcluedo.exception.ExisteDejaException;
import univorleans.fr.projetcluedo.exception.LoginDejaUtiliseException;
import univorleans.fr.projetcluedo.exception.UtlisateurPasConnecteException;
import univorleans.fr.projetcluedo.modele.*;

import javax.ws.rs.NotFoundException;
import java.util.*;

@Service
public class GestionCluedoImpl  implements GestionCluedoInterface, UserDetailsService {

    private static final String[] ROLES_USER     = {"USER"};
    private Map<String, Joueur> joueursConnectes = new HashMap<>() ;
    private Map<String, Partie> parties          = new HashMap<>();
    private Collection<Personnage> lesPersonnages= new ArrayList<>();

    @Autowired
    JoueurRepository joueurRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PartieRepository partieRepository;
    @Autowired
    private PersonnageRepository personnageRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    public GestionCluedoImpl( JoueurRepository joueurRepository,RoleRepository roleRepository) {

        this.joueursConnectes = new HashMap<>();
        this.joueurRepository = joueurRepository;
        bCryptPasswordEncoder=new BCryptPasswordEncoder();
        this.roleRepository=roleRepository;
    }

    public GestionCluedoImpl() {

    }

    @Override
    public Joueur inscription(Joueur joueur) {
        joueur.setMdp(bCryptPasswordEncoder.encode(joueur.getMdp()));
        Role r = this.roleRepository.findByRole("USER");
        joueur.setRoles(new HashSet<>(Arrays.asList(r)));
        joueurRepository.save(joueur);
        return joueur;
    }

    @Override
    public Joueur connexion(Joueur joueur) throws LoginDejaUtiliseException {
        if (this.joueursConnectes.containsKey(joueur.getPseudo()))
        {
            System.out.println("joueur deja connecté");
            throw new LoginDejaUtiliseException();
        }
        else
        {
            Joueur j=  joueurRepository.findByPseudo(joueur.getPseudo());
            this.joueursConnectes.put(joueur.getPseudo(), j);
            System.out.println("Création du joueur : "+j.getPseudo());
            loadUserByUsername(j.getPseudo());
            return j;
        }

    }

    @Override
    public Joueur deconnexion(Joueur joueur) {
        Joueur j=null;
        if(joueursConnectes.containsKey(joueur.getPseudo())) {

             j = this.joueursConnectes.remove(joueur.getPseudo());
        }
        return j;
    }



    @Override
    public Joueur findUserByPseudo(String pseudo) {
        return  joueurRepository.findByPseudo(pseudo);
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Joueur joueur = joueurRepository.findByPseudo(s);
        if(joueur != null) {
            String[] roles=ROLES_USER;
            UserDetails userDetails= User.builder()
                    .username(joueur.getPseudo())
                    .password(joueur.getMdp())
                    .roles(roles)
                    .build();
            return userDetails;
        } else {
            throw new UsernameNotFoundException("username not found");
        }

    }


    @Override
    public List<Joueur> getJoueurs() {
        return joueurRepository.findAll();
    }

    /**
     * retourne le joueur  actuellement Connecte
     *
     * @return
     */
    @Override
    public Joueur getCurrentUser()
    {

        if (this.joueursConnectes.size() == 1) {

            Map.Entry<String, Joueur> entry = joueursConnectes.entrySet().iterator().next();

            Joueur currentUser = entry.getValue();

            return currentUser;
        }
        return  null;
    }


    /**************************************PARTIE*************************************************/


    /**
     *cree une partie avec le joueur connecte comme maitre de partie
     *
     * @return
     */
    @Override
    public Partie creerPartie() {
        System.out.println(getCurrentUser());



        if (this.getCurrentUser() != null ){

            Joueur currentUser = this.getCurrentUser();

            Partie  newPartie = new Partie(currentUser.getPseudo());

            this.parties.put(currentUser.getPseudo(),newPartie);

            partieRepository.save(newPartie);
            return newPartie;
        }else
        {
            return  null;
        }


    }

    /**
     *retourne la partie id
     *
     * @param id
     * @return null || Optional<Partie>
     */
    @Override
    public Optional<Partie> getPartieById(int id)
    {
        return  partieRepository.findById(id);
    }


    /**
     *permet au maitre de parti d'inviter des joueurs
     *
     * @param id de la partie
     * @param pseudo de l'invite
     * @return
     */
    @Override
    public Partie ajoutJoueur(int id, String pseudo)
    {

        Partie findPartie  = partieRepository.findById(id).get();

        System.out.println("findPartie = " + findPartie);

        if (findPartie instanceof Partie && (findPartie.getEtat() != Partie.EtatPartie.SUSPENDED.toString() ||
                findPartie.getEtat() != Partie.EtatPartie.CANCELED.toString()
        )){ // verifie si la partie existe et est operationelle

            System.out.println(" = " +this.joueursConnectes);

            if (this.joueursConnectes.containsKey(findPartie.getPseudoMaitre())&&
//                     verifie si il s'aggit bien du maitre du jeu  et si il ne s'invite pas lui meme
                    pseudo != findPartie.getPseudoMaitre() ){

                if(findPartie.getJoueurs() !=null) {

                    int nbJoueur = findPartie.getJoueurs().size();

                    if (nbJoueur < 2 || nbJoueur < 5) {

                        Joueur guest = joueurRepository.findByPseudo(pseudo);

                        if (guest != null) {

                            Partie p = findPartie.addJoueur(guest);

                            partieRepository.save(findPartie);
                            return findPartie;

                        }
                    }
                }

            }

        }

        return null ;
    }



    /**
     * permet au joueur pseudo de choisir le personnage nomPersonnage
     *
     * @param idPartie
     * @param pseudo
     * @param nomPersonnage
     * @return
     */
    @Override
    public Partie choisirPersonnage(int idPartie, String pseudo ,String nomPersonnage)
    {

        Partie partie = partieRepository.findById(idPartie).get();

        if(partie!=null){

            if (isPresent(partie,pseudo)){


            }



        }

        return  null;
    }

    /**
     * permet de savoir si pseudo fait parti de la partie idPartie
     *
     * @param partie
     * @param pseudo
     * @return
     */
    private boolean isPresent(Partie partie, String pseudo)
    {
        boolean response = false;

        if (partie.getJoueurs().containsKey(pseudo) || partie.getPseudoMaitre() == pseudo)
        {
            response = true;
        }

        return  response ;
    }

    @Override
    public Personnage creerPersonnage(String nom) throws ExisteDejaException
    {

        System.out.println("lesPersonnages = " + lesPersonnages);

        if (this.lesPersonnages.contains(nom)){

            throw  new ExisteDejaException();

        }
        else
        {
            Personnage newPers = new  Personnage(nom);
            lesPersonnages.add(newPers);

            personnageRepository.save(newPers);
            return newPers;
        }
    }


    /**
     * permet a pseudo de rejoindre la partie id
     * @param id
     * @param pseudo du joueur actuel connecte
     *
     * @return
     */
    @Override
    public Partie rejoindrePartie(int id, String pseudo)
    {

        Partie partie = partieRepository.findById(id).get();

        if (this.joueursConnectes.containsKey(pseudo) && partie !=null){

            HashMap<String, JoueurKit> list = partie.getJoueurs();

            if(list.containsKey(pseudo)){

                JoueurKit newKit = new JoueurKit.KitBuilder(true).build();

                list.replace(pseudo,newKit);

                partie.setJoueurs(list);

                partieRepository.save(partie);
            }

            return  partie;
        }
        return  null;

    }


    /**
     *
     *
     * permet de changer l'etat de la partie en SUSPENDED si tous les joueurs ont donnes leur accord
     *
     * @param idPartie
     * @return
     */
    @Override
    public Partie sauvegarderPartie( int idPartie)
    {
        Partie partie = partieRepository.findById(idPartie).get();
        if (partie !=null){

            if (this.joueursConnectes.containsKey(partie.getPseudoMaitre())){ //si connecte en tant que maitre de la partie

                boolean response = false;
                ArrayList<Boolean> responses = new ArrayList<>();

                for (JoueurKit kit: partie.getJoueurs().values()) {

                    responses.add(kit.isSuspendre());

                    System.out.println(kit.isSuspendre());
                }
                if (!responses.contains(false)){

                    partie.setEtat(Partie.EtatPartie.SUSPENDED.toString());
                    partieRepository.save(partie);
                    return  partie;
                }
            }
        }

        return  null;
    }

    /**
     * permet au joueur pseudo de donner son accord ou son desaccord pour suspendre la partie
     *
     * @param idPartie
     * @param pseudo du joueur qui donne l'accord ou non
     */
    @Override
    public Partie setAccordJoueur(int idPartie ,String pseudo, boolean accord)
    {
        Partie partie = partieRepository.findById(idPartie).get();
        Joueur findJoueur = joueurRepository.findByPseudo(pseudo);

        if (joueursConnectes.containsKey(pseudo)) {

            if (partie != null && findJoueur != null) {

                if (partie.getJoueurs().containsKey(pseudo)) {

                    JoueurKit oldkit = partie.getJoueurs().get(pseudo);

                    if (oldkit.isRejoins()) {

                        JoueurKit newKit = new JoueurKit.KitBuilder(oldkit.isRejoins()).suspendreLeJeu(accord).build();

                        partie.getJoueurs().replace(pseudo, newKit);

                        partieRepository.save(partie);
                        return  partie;
                    }
                }

            }
        }
        return null;

    }

    /**
     * permet au maitre de la partie de reprendre le jeu l'etat repasse  a inprogress
     * @param idPartie
     * @return
     */
    @Override
    public  Partie reprendrePartie(int idPartie)
    {
        Partie partie = partieRepository.findById(idPartie).get();
        if (partie !=null){

            if (this.joueursConnectes.containsKey(partie.getPseudoMaitre())){

                partie.setEtat(Partie.EtatPartie.INPROGRESS.toString());

                for (String key: partie.getJoueurs().keySet()) {

                    System.out.println(key+": "+partie.getJoueurs().get(key));

                    JoueurKit oldKit = partie.getJoueurs().get(key);

                    JoueurKit newkit = new JoueurKit.KitBuilder(oldKit.isRejoins()).suspendreLeJeu(false).build();
                    partie.getJoueurs().replace(key,newkit);
                }

                partieRepository.save(partie);
                return  partie;
            }
        }

        return  null;
    }


    /**
     * retourne le pseudo du maitre de la partie
     *
     * @param idPartie
     * @return
     */
    @Override
    public String getMaitrePartie(int idPartie)
    {
        Partie partie = partieRepository.findById(idPartie).get();
        if (partie!=null){
            return  partie.getPseudoMaitre();
        }else {
            return  null;
        }

    }

    /**
     * retourne le nombre de joueur de la partie idpartie
     *
     * @param idPartie
     * @return
     */
    @Override
    public int getNbPartieJoueur(int idPartie)
    {
        Partie  partie = partieRepository.findById(idPartie).get();
        if (partie!=null){
            return partie.getJoueurs().size();
        }
        return 0;
    }

    /**
     * retourne l'etat de la partie idPartie
     * @param idPartie
     * @return
     */

    @Override
    public String getEtatPartie(int idPartie)
    {

        Partie  partie = partieRepository.findById(idPartie).get();
        if (partie!=null){

            return partie.getEtat();
        }
        return  null;
    }

    /**
     * retourne toutes les parties
     * @return
     */
    @Override
    public List<Partie> getParties()
    {
        return  partieRepository.findAll();
    }

    /**
     * @// TODO: 21/03/20  
     * @return
     */
    @Override
    public Collection<Partie> getPartiesInactif()
    {
        return null;
    }

    @Override
    public Partie getPartie(Joueur j)
    {
        return null;
    }

    /**
     * retourne les joueurs d'une partie
     *
     * @param idPartie
     * @return
     */
    @Override
    public Collection<Joueur> getJoueursPartie(int idPartie)
    {
        Partie  partie = partieRepository.findById(idPartie).get();

        if(partie!=null){
            Collection<Joueur> lesJoueurs= new ArrayList<>();

            for (String pseudo: partie.getJoueurs().keySet()) {

                Joueur joueur = joueurRepository.findByPseudo(pseudo);
                lesJoueurs.add(joueur);
            }

            return lesJoueurs;
        }

        return null;
    }


/**************************************PERSONNAGE*************************************************/

    /**
     * retourne le personnage avec l'id id
     * @param id
     * @return
     */
    @Override
    public Optional<Personnage> getPersoById(int id)
    {
        return personnageRepository.findById(id);
    }

    /**
     * retourne tous les personnages
     * @return
     */
    @Override
    public List<Personnage> getPersonnages()
    {
        return this.personnageRepository.findAll();
    }



}
