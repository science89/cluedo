package univorleans.fr.projetcluedo.service;


import org.springframework.stereotype.Service;
import univorleans.fr.projetcluedo.exception.ExisteDejaException;
import univorleans.fr.projetcluedo.exception.LoginDejaUtiliseException;
import univorleans.fr.projetcluedo.exception.UtlisateurPasConnecteException;
import univorleans.fr.projetcluedo.modele.Joueur;
import univorleans.fr.projetcluedo.modele.Partie;
import univorleans.fr.projetcluedo.modele.Personnage;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface GestionCluedoInterface {
    Joueur inscription(Joueur joueur);

    Joueur connexion(Joueur joueur) throws LoginDejaUtiliseException;

    Joueur deconnexion(Joueur joueur);


    String getEtatPartie(int idPartie);

    List<Partie> getParties();

    Collection<Partie> getPartiesInactif();


    Partie getPartie(Joueur j);

    Collection<Joueur> getJoueursPartie(int idPartie);

    String getMaitrePartie(int idPartie);

    int getNbPartieJoueur(int idPartie);

    Partie creerPartie();
    Joueur getCurrentUser();

    Partie ajoutJoueur(int id, String pseudo);

    Partie rejoindrePartie(int id, String pseudo);

    Partie sauvegarderPartie( int idPartie);
    Partie reprendrePartie(int idPartie);

    Partie setAccordJoueur(int idPartie ,String pseudo, boolean accord);

    Partie choisirPersonnage(int idPartie, String pseudo ,String nomPersonnage);

    List<Joueur> getJoueurs();

    Joueur findUserByPseudo(String email);

    Personnage creerPersonnage(String nom) throws ExisteDejaException;
    Optional<Partie> getPartieById(int id);
    List<Personnage> getPersonnages();
    Optional<Personnage> getPersoById(int id);

}
