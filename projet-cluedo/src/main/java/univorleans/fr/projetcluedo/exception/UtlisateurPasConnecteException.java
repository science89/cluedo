package univorleans.fr.projetcluedo.exception;

public class UtlisateurPasConnecteException extends  Exception {

    public UtlisateurPasConnecteException(String message) {
        super(message);
    }
}
