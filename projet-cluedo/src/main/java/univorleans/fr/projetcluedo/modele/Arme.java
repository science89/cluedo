package univorleans.fr.projetcluedo.modele;

public class Arme {

    private int idArme;
    private String nomArme;

    public int getIdArme() {
        return idArme;
    }

    public void setIdArme(int idArme) {
        this.idArme = idArme;
    }

    public String getNomArme() {
        return nomArme;
    }

    public void setNomArme(String nomArme) {
        this.nomArme = nomArme;
    }
}
