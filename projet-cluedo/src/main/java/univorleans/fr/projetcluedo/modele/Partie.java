package univorleans.fr.projetcluedo.modele;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Getter
@Setter
@Document(collection = "cluedo")
@JsonDeserialize
public class Partie implements Serializable
{
    private  static AtomicInteger counter = new AtomicInteger(1);


    @Id
    private int idPartie;
    private String pseudoMaitre;
    private String etat;
    private HashMap<String,JoueurKit> joueurs ;

    /** toutes les valeurs de l'etat d'une partie  **/

    public enum EtatPartie{CREATED,INPROGRESS,SUSPENDED,ENDED,CANCELED}


    public Partie(String pseudoMaitre) {

        this();
        this.pseudoMaitre = pseudoMaitre;
        this.joueurs = new HashMap<String, JoueurKit>();

        this.etat = EtatPartie.CREATED.toString();
    }


    public Partie(){
        this.idPartie = counter.getAndIncrement();
    }

    public Partie addJoueur(Joueur joueur)
    {
        if (!joueurs.containsKey(joueur.getPseudo())) {

            joueurs.put(joueur.getPseudo(),new JoueurKit.KitBuilder(false).build());
            System.out.println(joueurs);

        }

        return this;
    }

}
