package univorleans.fr.projetcluedo.modele;

import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@Document(collection = "cluedo")
public class Carte implements ICarte{



    public enum TypeCarte{QC,QCP,OU,SPECIAL}


        private TypeCarte type ;

    @Override
    public TypeCarte getType() {
        return this.type;
    }

    public void setType(TypeCarte type) {
        this.type = type;
    }
}
