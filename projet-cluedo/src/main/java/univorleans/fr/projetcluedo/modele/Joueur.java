package univorleans.fr.projetcluedo.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@Getter
@Setter

@Document(collection="joueur")
public class Joueur  {

    private  static AtomicInteger counter = new AtomicInteger(1);

    @Id
    private int idJoueur;
    private String email;
    private String pseudo;
    private String mdp;
    @DBRef
    private Set<Role> roles;


    public Joueur(String email, String pseudo, String mdp) {
        this();
        this.email=email;
        this.mdp=mdp;
        this.pseudo=pseudo;
    }
    public Joueur()
    {
        this.idJoueur=counter.getAndIncrement();
    }
}
