package univorleans.fr.projetcluedo.modele;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
/**
 * cet Objet reuni toutes les options  neccessaire a UN joueur pendant une partie
 *
 */
public class JoueurKit {

    private boolean rejoins;
    private boolean suspendre;
    private int perso;

    private JoueurKit(KitBuilder kitBuilder){

        this.rejoins= kitBuilder.isRejoinsJeu();
        this.suspendre= kitBuilder.isSuspendreJeu();
        this.perso =kitBuilder.getIdPersonnage();

    }

    @Getter
    @Setter
    public static class KitBuilder{

        private boolean rejoinsJeu;
        private boolean suspendreJeu;
        private int idPersonnage;

        public KitBuilder(boolean rejoinsJeu)
        {
            this.rejoinsJeu=rejoinsJeu;
        }

        public KitBuilder suspendreLeJeu(boolean accord)
        {
            this.suspendreJeu = accord;

            return  this;
        }

        public KitBuilder personnage(int idPersonnage)
        {
            this.idPersonnage = idPersonnage;
            return  this;
        }


        public JoueurKit build()
        {
            return new JoueurKit(this);

        }
    }

}
