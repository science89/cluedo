package univorleans.fr.projetcluedo.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
@Document(collection = "personnage")
public class Personnage  {

    private  static AtomicInteger counter = new AtomicInteger(1);

    @Id
    private int idPersonnage ;

    private String nom;



/** valeurs des noms de tous les personnages du jeu **/

    public enum NomPersonnage{PROFVIOLET,DOCORCHIDEE,COLONELMOUTARDE,MLLEROSE,MMEPERVENCHE,MROLIVE}

    public Personnage(String nom) {
        this();

        this.nom = nom;
    }

    public Personnage() {
        this.idPersonnage = counter.getAndIncrement();

    }
}
