package univorleans.fr.projetcluedo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import univorleans.fr.projetcluedo.exception.ExisteDejaException;
import univorleans.fr.projetcluedo.modele.Partie;
import univorleans.fr.projetcluedo.modele.Personnage;
import univorleans.fr.projetcluedo.service.GestionCluedoInterface;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/apicluedo")
public class PartieController {

    private GestionCluedoInterface service;

    public PartieController( GestionCluedoInterface service) {
        this.service= service;

    }


    /**
     * Permet de récupérer la liste de toutes les parties
     * @return
     */
    @GetMapping(value="/partie", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Partie>> getAllPartie() {

        return ResponseEntity.ok().body(service.getParties());
    }

    /**
     * Permet de créer une partie
     * @param partie
     * @return
     */

    @PostMapping(value="/partie")
    public ResponseEntity<Partie> createPartie(@RequestBody Partie partie) {

        Partie p =  service.creerPartie();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(p.getIdPartie()).toUri();

        return ResponseEntity.created(location).body(p);

    }

    /**
     * Permet d'ajouter un joueur à la liste de de joueurs
     * se connecter en tant que maitre de la partie au prealable
     *
     * @param id
     * @param pseudo
     * @return
     */
    @PutMapping(value="/partie/{id}/ajoutJoueur/{pseudo}")
    public ResponseEntity<Partie> ajoutJoueur(@PathVariable int id, @PathVariable("pseudo") String pseudo) {

        Partie  updatedPartie = service.ajoutJoueur(id,pseudo);

        if (updatedPartie != null) {

            return ResponseEntity.ok().body(updatedPartie);
        }
        else {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

        }
    }


    /**
     * Permet de récupérer une partie avec son id
     * @param id
     * @return
     */
    @GetMapping(value="/partie/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<Partie>> getPartieById(@PathVariable  int id) {

        Optional<Partie>  p = service.getPartieById(id);
        if (p!=null) {
            return ResponseEntity.ok().body(p);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    /**
     * Permet a un joueur invite a la partie id de rejoindre la partie
     *
     * @param id
     * @param pseudo
     * @return
     */
    @PutMapping(value="/partie/{id}/rejoindre/{pseudo}")
    public ResponseEntity<Partie> rejoindrePartie(@PathVariable("id") int id, @PathVariable String pseudo) {

        Partie partie = service.rejoindrePartie(id, pseudo);

        if (partie instanceof Partie) {
            return ResponseEntity.ok().body(partie);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    /**
     * Permet a un joueur de donner l'accord ou pas de suspendre le jeu
     *
     * @return
     */
//    @PutMapping(value="/partie/{id}/accordSuspendre" , consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Partie> accordSuspendreJeu(@RequestBody Map<String, String> json, @PathVariable("id") int id) {
//        boolean ac=false;
//        System.out.println(json.get("accord"));
//        System.out.println(json.get("pseudo"));
//        if(json.get("accord").equals("true")){
//            ac=true;
//        }
//        System.out.println(ac);
//        Partie partie = service.setAccordJoueur(id, json.get("pseudo"),ac);
//        if (partie instanceof Partie) {
//           return ResponseEntity.ok().body(partie);
//        } else {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
//        }
//    }


    @PutMapping(value="/partie/{id}/accordSuspendre/{pseudo}/{accord}")
    public ResponseEntity<Partie> accordSuspendreJeu(@PathVariable("id") int id,@PathVariable("pseudo")
            String pseudo, @PathVariable("accord") boolean accord)
    {

        Partie partie = service.setAccordJoueur(id, pseudo,accord);

        if (partie instanceof Partie) {
            return ResponseEntity.ok().body(partie);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    /**
     * Permet de passer l'etat de la partie a SUSPENDED
     * se connecter en tant que maitre de la partie au prealable

     * @param id
     * @return
     */
    @PutMapping(value="/partie/{id}/sauvegarder")
    public ResponseEntity<Partie> sauvegarderJeu(@PathVariable("id") int id) {

        Partie partie = service.sauvegarderPartie(id);

        if (partie instanceof Partie) {
            return ResponseEntity.ok().body(partie);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Permet de reprendre la partie
     * se connecter en tant que maitre de la partie au prealable
     * @param id
     * @return
     */
    @PutMapping(value="/partie/{id}/reprendre")
    public ResponseEntity<Partie> reprendreJeu(@PathVariable("id") int id) {

        Partie partie = service.reprendrePartie(id);

        if (partie instanceof Partie) {
            return ResponseEntity.ok().body(partie);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * permet de creer un personnage voir la classe Personnage pour les valeurs de nom
     *
     * @param nom
     * @return
     * @throws ExisteDejaException
     */

    @PostMapping(value="/personnage")
    public ResponseEntity<Personnage> creerPersonnage(@RequestBody String nom) throws ExisteDejaException {


        Personnage p =  service.creerPersonnage(nom);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(p.getIdPersonnage()).toUri();

        return ResponseEntity.created(location).body(p);

    }

    /**
     * Permet de récupérer tous les personnages
     * @return null || List<Personnage>
     */
    @GetMapping(value="/personnage", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Personnage>> getAllPerso() {
        return ResponseEntity.ok().body(service.getPersonnages());
    }

    /**
     * Permet de récupérer un personnage par son id
     * @param id
     * @return
     */
    @GetMapping(value="/personnage/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<Personnage>> getPersoById(@PathVariable  int id) {

        Optional<Personnage>  p = service.getPersoById(id);

        if (p!=null) {
            return ResponseEntity.ok().body(p);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


}
