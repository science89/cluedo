package univorleans.fr.projetcluedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import univorleans.fr.projetcluedo.dao.JoueurRepository;
import univorleans.fr.projetcluedo.exception.LoginDejaUtiliseException;
import univorleans.fr.projetcluedo.modele.Joueur;
import univorleans.fr.projetcluedo.service.GestionCluedoImpl;
import univorleans.fr.projetcluedo.service.GestionCluedoInterface;

import javax.print.attribute.standard.JobOriginatingUserName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apicluedo")
public class JoueurController {
    private GestionCluedoInterface service;

    @Autowired
    public JoueurController(GestionCluedoInterface service) {
        this.service=service;
    }
    @PostMapping("/addjoueur")
    public  ResponseEntity<Joueur> saveJoueur(@RequestBody Joueur joueur) {
        service.inscription(joueur);
        return ResponseEntity.ok().body(joueur);
    }
   @PostMapping ("/login")
    public ResponseEntity<Joueur> login (@RequestBody Joueur joueur) throws LoginDejaUtiliseException {
       Joueur j= service.connexion(joueur);
        return ResponseEntity.ok().body(j);
    }

    @PostMapping ("/logout")
    public ResponseEntity<Joueur> logout (@RequestBody Joueur joueur, HttpServletRequest request, HttpServletResponse response) throws LoginDejaUtiliseException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        Joueur j=service.deconnexion(joueur);
        return ResponseEntity.ok().body(j);
    }

    @GetMapping("/findAllJoueurs")
    public List<Joueur> getJoueurs() {
        return service.getJoueurs();
    }

  /*  @GetMapping("/findAllJoueurs/{id}")
    public Optional<Joueur> getJoueur(@PathVariable int id) {
        return ;
    }*/

 /*   @DeleteMapping("/delete/{id}")
    public String deleteJoueur(@PathVariable int id) {
        repository.deleteById(id);
        return "joueur deleted with id : " + id;
    }*/

}
