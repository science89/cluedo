/**
 *
 */
module clientJFX {

    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.base;
    requires javafx.graphics;
    requires com.fasterxml.jackson.core;
    requires spring.boot;
    requires spring.context;
    requires java.net.http;
    requires com.fasterxml.jackson.annotation;
    requires jackson.databind;

    opens cluedo.application to javafx.fxml;
    opens cluedo.application.vues to javafx.fxml;
    exports cluedo.application;
    exports cluedo.application.vues;

}
