package cluedo.application.vues;

import cluedo.application.controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;

public class JouerPartie {
    Controller monControleur;
    private Stage stage;

    @FXML
    AnchorPane topNiveau;

    @FXML
    private javafx.scene.control.Label username;

    public Label getUsername() { return username; }

    public static JouerPartie creerEtAfficher(Controller controller, Stage stage) {
        URL location = JouerPartie.class.getResource("/vues/jouerPartie.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        Parent root = null;
        try {
            root = (Parent) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JouerPartie vue = fxmlLoader.getController();
        stage.setTitle("Inviter");
        stage.setScene(new Scene(root, 600, 400));
        stage.show();
        vue.setMonController(controller);
        return vue;
    }
    public void setMonController(Controller monControleur) {
        this.monControleur = monControleur;
    }

    public void show(Stage stage){
        stage.setTitle("Invitations des joueurs");
        //stage.setScene(new Scene(topNiveau,600,400));
        stage.show();
    }

    public void inviterJoueur(ActionEvent actionEvent) {
        monControleur.gotoInviter();
    }

    public void seDeconnecter(ActionEvent actionEvent) {
        monControleur.quitter();
    }
}
