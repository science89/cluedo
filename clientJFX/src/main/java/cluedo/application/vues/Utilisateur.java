package cluedo.application.vues;

import cluedo.application.controller.Controller;
import cluedo.application.services.dto.UtilisateurDTO;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Utilisateur {
    private static int IDSUTIL=1;
    private int idUtil;
    private String pseudo;
    private String password;
    private Controller monControleur;
    private Stage stage;


    private UtilisateurDTO util;

    @FXML
    AnchorPane topNiveau;

    public Utilisateur() { this.idUtil=IDSUTIL++; }

    public int getIdUtil() { return idUtil; }

    public Utilisateur(String pseudo, String password) {
        this();
        this.pseudo = pseudo;
        this.password = password;
    }

    public void setIdUtil(int idUtil) { this.idUtil = idUtil; }

    public String getPseudo() {
        return pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean checkPassword(String password){
        return this.password.equals(password);
    }

    public static Utilisateur create(String login,String mdp){
        return new Utilisateur(login,mdp);
    }


}
