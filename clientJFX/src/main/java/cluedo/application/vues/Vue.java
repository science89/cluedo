package cluedo.application.vues;

import cluedo.application.controller.Controller;

public interface Vue {
    void setControleur(Controller controleur);
    void show();
}
