package cluedo.application.vues;

import cluedo.application.controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;

public class Accueil {
    
    private Stage stage ;
    private Controller controller;

    @FXML
    AnchorPane topNiveau;
    @FXML
    AnchorPane root;

    public static Accueil creerInstance(Controller controller, Stage stage) {
        URL location = Accueil.class.getResource("/vues/accueil.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        AnchorPane root = null;
        try {
            root = (AnchorPane) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();

        }
        Accueil vue = fxmlLoader.getController();
        //stage1.setTitle("Connexion");
        //laStageUnique.setScene(new Scene(root, 800, 600));
        vue.setStage(stage);
        //stage1.show();
        vue.setMonController(controller);
        return vue;
    }


    private void setStage(Stage stage1) {
        this.stage=stage1;
    }

    private void setMonController(Controller c) {
        this.controller=c;
    }

    public void show(){
        stage.setTitle("CLUEDO GAME");
        stage.setScene(new Scene(topNiveau,700,480));
        stage.show();
    }




    public void gotoLogin(ActionEvent event) {
        controller.gotoLogin();
    }

    public void gotoInscription(ActionEvent event) {
        controller.gotoInscription();
    }
}
