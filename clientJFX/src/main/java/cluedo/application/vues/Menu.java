package cluedo.application.vues;

import cluedo.application.controller.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;

public class Menu {

    Controller monControleur;
    private Stage stage;

    @FXML
    AnchorPane topNiveau;
    @FXML
    private javafx.scene.control.Label username;

    public Label getUsername() { return username; }

    public static Menu creerEtAfficher(Controller controller, Stage stage) {
        URL location = Menu.class.getResource("/vues/menu.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        Parent root = null;
        try {
            root = (Parent) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Menu vue = fxmlLoader.getController();
        stage.setTitle("Menu");
        stage.setScene(new Scene(root, 600, 400));
        stage.show();
        vue.setMonController(controller);
        return vue;
    }
    public void setMonController(Controller monControleur) {
        this.monControleur = monControleur;
    }

    public void show(Stage stage){
        stage.setTitle("Menu Principal");
        //stage.setScene(new Scene(topNiveau,600,400));
        stage.show();
    }

    public void quitterOk(ActionEvent actionEvent) {
        monControleur.run();
    }

    public void creerPartie(ActionEvent actionEvent) {
        monControleur.gotoJouerPartie();
    }

    public void restaurerPartie(ActionEvent actionEvent) {
    }

    public void rejoindrePartie(ActionEvent actionEvent) {

    }
}
