package cluedo.application.vues;

import cluedo.application.controller.Controller;
import cluedo.application.services.CluedoInterface;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;

public class Login {

    private Controller monControleur;
    private Stage stage;
    private CluedoInterface interfaceUtilisateur;

    @FXML
    private TextField pseudo;

    @FXML
    private PasswordField password;

    @FXML
    AnchorPane topNiveau;

    public TextField getPseudo() { return pseudo; }

    public static Login creerEtAfficher(Controller c, Stage stage) {
        URL location = Login.class.getResource("/vues/connexion.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        AnchorPane root = null;
        try {
            root = (AnchorPane) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Login vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setMonController(c);
        return vue;
    }

    private void setStage(Stage stage) {
        this.stage = stage;
    }

    public CluedoInterface getFacadeI(){
        return interfaceUtilisateur;
    }

    public void show(){
        stage.setTitle("Page de connnexion");
        stage.setScene(new Scene(topNiveau,645,426));
        stage.show();
    }
    private void setMonController(Controller c) {
        this.monControleur=c;
    }



    public void afficheMessageErreur(String s) {
        Alert a = new Alert(Alert.AlertType.ERROR,s, ButtonType.OK);
        a.show();
    }

    public void bntConnexion(ActionEvent event) {
        boolean condition = pseudo.getText() == null || pseudo.getText().length() == 0 || password.getText() == null || password.getText().length() == 0;
        if (condition) {
            Alert a = new Alert(Alert.AlertType.ERROR,"Les champs login/password sont obligatoires !", ButtonType.OK);
            a.show();
        }
        else {
            monControleur.tenteUnLogin(pseudo.getText(), password.getText());
        }
    }

    private void showMessage(String message) {
        Alert a = new Alert(Alert.AlertType.ERROR,"couple/mot de passe invalide", ButtonType.OK);
            a.setTitle("Erreur");
            a.setContentText(message);
            a.showAndWait();
    }

    public void btnRetour(ActionEvent event) {
        monControleur.goToMenu();
    }

}
