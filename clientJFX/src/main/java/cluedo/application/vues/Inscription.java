package cluedo.application.vues;

import cluedo.application.controller.Controller;
import cluedo.application.exceptions.ExceptionPseudoDejaPris;
import cluedo.application.exceptions.InformationsSaisiesIncoherentesException;
import cluedo.application.exceptions.PseudoDejaUtiliseException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class Inscription implements Vue{
    private Controller monControleur;
    private Stage stage;

    @FXML
    private TextField pseudo;

    @FXML
    private PasswordField password;

    @FXML
    private PasswordField password1;

    @FXML
    private Button valider;

    @FXML
    AnchorPane topNiveau;

    public static Inscription creerInscription(Controller c, Stage stage) {
        URL location = Inscription.class.getResource("/vues/inscription.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        AnchorPane root = null;
        try {
            root = (AnchorPane) fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Inscription vue = fxmlLoader.getController();
        vue.setStage(stage);
        vue.setMonController(c);
        return vue;
    }

    private void setStage(Stage stage) {
        this.stage = stage;
    }


    @Override
    public void setControleur(Controller controleur) {
    }

    public void show(){
        stage.setTitle("Page d'inscription");
        stage.setScene(new Scene(topNiveau,600,400));
        stage.show();
    }
    private void setMonController(Controller c) {
        this.monControleur=c;
    }

    public void inscription(ActionEvent actionEvent) {
        String pseud = pseudo.getText();
        String pwd = password.getText();
        String cpwd = password1.getText();

        if (pseud.length() < 2 || (pwd.length() < 2) || (cpwd.length() < 2) ||(!pwd.equals(cpwd))){
            showMessage("Donnée incorrecte");
            monControleur.gotoInscription();
        }else{
            try {
                monControleur.tenteUneInscription(pseud,pwd,cpwd);
            } catch (PseudoDejaUtiliseException e) {
                showMessage("Utilisateur déjà connecté");
            }catch (InformationsSaisiesIncoherentesException e){
                showMessage("information incohérente");

            }
        }


    }

    private void showMessage(String message) {
        Alert a = new Alert(Alert.AlertType.ERROR,"couple/mot de passe invalide", ButtonType.OK);
        a.setTitle("Erreur de saisi");
        a.setContentText(message);
        a.setHeaderText("S'il vous plait corriger");
        a.setContentText(message);
        a.showAndWait();
    }

}
