package cluedo.application.services;

import cluedo.application.exceptions.ExceptionPseudoDejaPris;
import cluedo.application.exceptions.InformationsSaisiesIncoherentesException;
import cluedo.application.exceptions.PseudoDejaUtiliseException;
import cluedo.application.vues.Inscription;
import cluedo.application.vues.Utilisateur;
import cluedo.application.services.dto.UtilisateurDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CluedoProxy implements CluedoInterface {

    private static final String URI_SERVICE="http://localhost:8080/cluedo9";

    private static final String UTILISATEUR = "/utilisateur";

    private HttpClient httpClient = HttpClient.newHttpClient();

    private ObjectMapper objectMapper = new ObjectMapper();

    List<String> pseudosConnectes;
    Map<String, Utilisateur> utilisateursMap;
    Map <String, Inscription> inscriptionMap;

    public CluedoProxy() {
        pseudosConnectes = new ArrayList<>();
        utilisateursMap = new HashMap<>();
        this.initialiserDAO();

    }

    private void initialiserDAO() {
        this.utilisateursMap.put("science", new Utilisateur("science", "science"));
        this.utilisateursMap.put("saliou", new Utilisateur("saliou", "saliou"));
    }


    /**
     * Permet de se connecter à l'application si le couple pseudo/mdp est cohérent
     *
     * @param pseudo
     * @param mdp
     * @throws PseudoDejaUtiliseException
     * @throws InformationsSaisiesIncoherentesException
     */
    @Override
    public void connexion(String pseudo, String mdp) throws PseudoDejaUtiliseException, InformationsSaisiesIncoherentesException {
        if (pseudosConnectes.contains(pseudo))
            throw new PseudoDejaUtiliseException();

        Utilisateur u = this.utilisateursMap.get(pseudo);
        if (u.checkPassword(mdp)) {
            this.pseudosConnectes.add(pseudo);
            return;
        }
        throw new InformationsSaisiesIncoherentesException();
    }

    @Override
    public Utilisateur inscription(String pseudo, String mdp, String confMdp) throws PseudoDejaUtiliseException, InformationsSaisiesIncoherentesException {

        if(pseudosConnectes.contains(pseudo)) {
            throw new PseudoDejaUtiliseException();
        }else {
            utilisateursMap.put(pseudo, new Utilisateur(pseudo, mdp));
        }

        System.out.println(utilisateursMap);
        System.out.println(pseudosConnectes);
        return utilisateursMap.get(pseudo);
    }

    /**
     * Permet de déconnecter proprement l'utilisateur (pseudo) de l'application
     *
     * @param pseudo
     */
    @Override
    public void deconnexion(String pseudo) {
        this.pseudosConnectes.remove(pseudo);
    }

    @Override
    public String creerUtilisateur(String pseudo, String password) throws ExceptionPseudoDejaPris {
        UtilisateurDTO utilisateurDTO = UtilisateurDTO.creer(pseudo, password);
        try {
            String utilisateurDTOJSON = objectMapper.writeValueAsString(utilisateurDTO);
            HttpRequest httpRequest = HttpRequest.newBuilder(URI.create(URI_SERVICE+UTILISATEUR))
                    .header("Content-Type","application/json").POST(HttpRequest.BodyPublishers.ofString(utilisateurDTOJSON)).build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() == 201) {
                String pseudoConfirme = response.headers().firstValue("pseudo").get();
                return pseudoConfirme;
            }

            if (response.statusCode() == 409) {
                throw new ExceptionPseudoDejaPris();
            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UtilisateurDTO getUtilisateur(String pseudo) {
        return null;
    }


}
