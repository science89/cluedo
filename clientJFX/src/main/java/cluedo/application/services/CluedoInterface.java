package cluedo.application.services;

import cluedo.application.exceptions.ExceptionPseudoDejaPris;
import cluedo.application.exceptions.InformationsSaisiesIncoherentesException;
import cluedo.application.exceptions.PseudoDejaUtiliseException;
import cluedo.application.services.dto.UtilisateurDTO;
import cluedo.application.vues.Utilisateur;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface CluedoInterface {
    /**
     * Permet de se connecter à l'application si le couple pseudo/mdp est cohérent
     * @param pseudo
     * @param mdp
     * @throws PseudoDejaUtiliseException
     * @throws InformationsSaisiesIncoherentesException
     */
    void connexion(String pseudo, String mdp) throws PseudoDejaUtiliseException, InformationsSaisiesIncoherentesException, PseudoDejaUtiliseException, InformationsSaisiesIncoherentesException;

    Utilisateur inscription(String pseudo, String mdp, String confMdp) throws PseudoDejaUtiliseException, InformationsSaisiesIncoherentesException;



    /**
     * Permet de déconnecter proprement l'utilisateur (pseudo) de l'application
     * @param pseudo
     */
    void deconnexion(String pseudo);

    String creerUtilisateur(String pseudo, String password) throws ExceptionPseudoDejaPris;
    UtilisateurDTO getUtilisateur(String pseudo);
}
