package cluedo.application.services.dto;

public class UtilisateurDTO {
    private int idUtil;
    private String pseudo;
    private String password;

    public UtilisateurDTO() { }

    public static UtilisateurDTO creer(String pseudo, String password) {
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();
        utilisateurDTO.setPseudo(pseudo);
        utilisateurDTO.setPassword(password);
        return utilisateurDTO;
    }

    public int getIdUtil() { return idUtil; }

    public void setIdUtil(int idUtil) { this.idUtil = idUtil; }

    public String getPseudo() { return pseudo; }

    public void setPseudo(String pseudo) { this.pseudo = pseudo; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

}
