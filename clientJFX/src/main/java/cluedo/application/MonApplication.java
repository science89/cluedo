package cluedo.application;

import cluedo.application.controller.Controller;
import cluedo.application.vues.Accueil;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ConfigurableApplicationContext;


public class MonApplication extends Application {

    private Stage primaryStage;
    private Controller controller;
    private Accueil accueil;
    private ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) { MonApplication.launch(args); }

   /* @Override
    public void start(Stage primaryStage) throws Exception {
           controller = new Controller(primaryStage);
           controller.run();
    }*/

    /**
     * The main entry point for all JavaFX applications.
     * The start method is called after the init method has returned,
     * and after the system is ready for the application to begin running.
     *
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param primaryStage the primary stage for this application, onto which
     *                     the application scene can be set.
     *                     Applications may create other stages, if needed, but they will not be
     *                     primary stages.
     * @throws Exception if something goes wrong
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        controller = new Controller(primaryStage);
        controller.run();
    }

   /* @Override
    public void stop()  {
        applicationContext.close();
        Platform.exit();
    }*/

   /* @Override
    public void init() throws Exception { applicationContext= new SpringApplicationBuilder(MonApplication.class).run();
    }*/

   /* static class StageReadyEvent extends ApplicationEvent {
        public StageReadyEvent(Stage stage) {
            super(stage);
        }

        public Stage getStage() {
            return ((Stage) getSource());
        }
    }*/




}
