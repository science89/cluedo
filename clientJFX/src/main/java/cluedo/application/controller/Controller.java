package cluedo.application.controller;

import cluedo.application.exceptions.ExceptionPseudoDejaPris;
import cluedo.application.exceptions.InformationsSaisiesIncoherentesException;
import cluedo.application.exceptions.PseudoDejaUtiliseException;
import cluedo.application.vues.Utilisateur;
import cluedo.application.services.CluedoInterface;
import cluedo.application.services.CluedoProxy;
import cluedo.application.vues.*;
import javafx.stage.Stage;




public class Controller {

    private Stage primaryStage;
    private Login login;
    private Menu menu;
    private JouerPartie jouerPartie;
    private Inviter inviter;
    private CluedoInterface facade;
    private Accueil accueil;
    private Utilisateur utilisateur;
    private Controller controller;
    private Inscription inscription;
    private String utilisateurDTO;

    String pseudo;
    String motDepasse;

    public Controller(Stage primaryStage) {
        this.primaryStage = primaryStage;
        facade = new CluedoProxy();
    }

    // Pour se connecter sur le jeu
    public void tenteUnLogin(String pseudo, String motDePasse) {
        try {
            facade.connexion(pseudo,motDePasse);
            // OK on est log
            this.pseudo = pseudo;
            this.motDepasse = motDePasse;
            this.goToMenu();
            // afficher le menu
            // changer la Scene de la stage pour le menu
        } catch (PseudoDejaUtiliseException exceptionLoginDejaPris) {
            // pas OK : on reste sur le login
            login.afficheMessageErreur("Login déjà connecté !!!");
        } catch (Exception e) {
            login.afficheMessageErreur("Couple Login/mot de passe incohérent");
        }
    }

    /*public String creerUtilisateur(String pseudo, String password) throws ExceptionPseudoDejaPris {
        this.pseudo =  this.facade.creerUtilisateur(pseudo,password);
        //this.goToMenu();
        return this.pseudo;
    }*/

    public String creerUtilisateur(String pseudo, String password) throws ExceptionPseudoDejaPris {
        this.utilisateurDTO =  this.facade.creerUtilisateur(pseudo,password);
        //this.goToMenu();
        return this.utilisateurDTO;
    }


    // gotoMenuPrincipal();
    public void goToMenu() {
        menu = Menu.creerEtAfficher(this, this.primaryStage);
        menu.show(this.primaryStage);
        menu.getUsername().setText(String.valueOf(login.getPseudo().getText()));
    }


    public void quitter() {
        this.facade.deconnexion(pseudo);
        this.login = Login.creerEtAfficher(this,primaryStage);
        login.show();

    }

    public void gotoLogin() {
        login=Login.creerEtAfficher(this,primaryStage);
        login.show();
    }

    public void gotoInscription() {
        //inscription = Inscription.creerInscription(this,primaryStage);
        //inscription.show();
        inscription = Inscription.creerInscription(this,primaryStage);
        inscription.show();


    }

    public void gotoInviter(){
        inviter = Inviter.creerEtAfficher(this,primaryStage);
        inviter.show(this.primaryStage);
        inviter.getUsername().setText(String.valueOf(login.getPseudo().getText()));
    }

    public void gotoJouerPartie(){
        jouerPartie = JouerPartie.creerEtAfficher(this,primaryStage);
        jouerPartie.show(this.primaryStage);
        jouerPartie.getUsername().setText(String.valueOf(login.getPseudo().getText()));
    }

    public void run() {
        accueil = Accueil.creerInstance(this,primaryStage);
        accueil.show();
    }

    public void tenteUneInscription(String pseudo, String motDepasse, String confirmationMotDePasse) throws PseudoDejaUtiliseException, InformationsSaisiesIncoherentesException{
        try {
            utilisateur = facade.inscription(pseudo,motDepasse,confirmationMotDePasse);
            this.gotoLogin();

            // afficher le menu
            // changer la Scene de la stage pour le menu
        } catch (PseudoDejaUtiliseException e) {
            e.printStackTrace();
        } catch (InformationsSaisiesIncoherentesException e) {
            e.printStackTrace();
        }

    }


}
