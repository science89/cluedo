package cluedo.application.exceptions;

public class UnexpectedError extends RuntimeException {
    public UnexpectedError(String s) {
        super(s);
    }

    public UnexpectedError() {
    }
}
